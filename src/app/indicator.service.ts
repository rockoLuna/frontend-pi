import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Respuesta } from './model/Respuesta';
import { RespuestaTesisIcr } from './model/RespuestaTesisIcr';


@Injectable({
  providedIn: 'root'
})
export class IndicatorService {

  constructor(private client: HttpClient) { }
  private host = "http://localhost:7080";

  getIndicators(): Observable<Respuesta> {
    const api = '/api-indicadores/search/all';
    const url =this.host+api;
    return this.client.get<Respuesta>(url);
  }

  searchIndicators(level:String, type:number): Observable<Respuesta> {
    const api = '/api-indicadores/search/academic';
    const url =this.host+api;
    return this.client.post<Respuesta>(url, {
      "nivel": level,
      "tipoIndicador": type
    });
  }

  searchProfessorIndicators(type:number): Observable<Respuesta> {
    const api = '/api-indicadores/search/professor';
    const url =this.host+api;
    return this.client.post<Respuesta>(url, {
      "tipoIndicador": type
    });
  }

  searchTesisIcrIndicators(level:String, type:number): Observable<RespuestaTesisIcr> {
    const api = '/api-indicadores/search/tesisIcr';
    const url =this.host+api;
    return this.client.post<RespuestaTesisIcr>(url, {
      "nivel": level,
      "tipoIndicador": type
    });
  }
}
  
