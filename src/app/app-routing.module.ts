import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AspirantesComponent } from './components/graphs/aspirantes/aspirantes.component';
import { BarChartComponent } from './components/graphs/bar-chart/bar-chart.component';
import { EficienciaComponent } from './components/graphs/eficiencia/eficiencia.component';
import { IcrsComponent } from './components/graphs/icrs/icrs.component';
import { IcryTesisComponent } from './components/graphs/icry-tesis/icry-tesis.component';
import { IngresoEgresoComponent } from './components/graphs/ingreso-egreso/ingreso-egreso.component';
import { NumeroProfesoresComponent } from './components/graphs/numero-profesores/numero-profesores.component';
import { PerfilesProfesoresComponent } from './components/graphs/perfiles-profesores/perfiles-profesores.component';
import { ProduccionProfesoresComponent } from './components/graphs/produccion-profesores/produccion-profesores.component';
import { ProduccionComponent } from './components/graphs/produccion/produccion.component';
import { PromedioComponent } from './components/graphs/promedio/promedio.component';
import { TesisComponent } from './components/graphs/tesis/tesis.component';
import { TiempoIcrTesisComponent } from './components/graphs/tiempo-icr-tesis/tiempo-icr-tesis.component';
import { HomeComponent } from './components/home/home.component';
import { MenuDoctoradoComponent } from './components/menus/menu-doctorado/menu-doctorado.component';
import { MenuGlobalComponent } from './components/menus/menu-global/menu-global.component';
import { MenuMaestriaComponent } from './components/menus/menu-maestria/menu-maestria.component';
import { MenuProfesoresComponent } from './components/menus/menu-profesores/menu-profesores.component';
import { MenuTesisComponent } from './components/menus/menu-tesis/menu-tesis.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'menuMaestria', component: MenuMaestriaComponent},
  {path: 'menuDoctorado', component: MenuDoctoradoComponent},
  {path: 'menuGlobal', component: MenuGlobalComponent},
  {path: 'menuProfesores', component: MenuProfesoresComponent},
  {path: 'menuTesis', component: MenuTesisComponent},
  {path: 'IngresoEgreso/:level', component: IngresoEgresoComponent},
  {path: 'Eficiencia/:level', component: EficienciaComponent},
  {path: 'Aspirantes/:level', component: AspirantesComponent},
  {path: 'Produccion/:level', component: ProduccionComponent},
  {path: 'Promedio/:level', component: PromedioComponent},
  {path: 'numeroProfesores', component: NumeroProfesoresComponent},
  {path: 'produccionProfesores', component: ProduccionProfesoresComponent},
  {path: 'perfilProfesores', component: PerfilesProfesoresComponent},
  {path: 'icrs', component: IcrsComponent},
  {path: 'tesis', component: TesisComponent},
  {path: 'icrTesis', component: IcryTesisComponent},
  {path: 'tiempoIcrTesis', component: TiempoIcrTesisComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
