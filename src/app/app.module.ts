import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { IndicatorsComponent as IndicatorsComponent } from './components/indicators/indicators.component';
import { IndicatorComponent } from './components/indicator/indicator.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';3
import {MatSidenavModule} from '@angular/material/sidenav';
import { MenuComponent } from './components/menus/menu/menu.component';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatGridListModule} from '@angular/material/grid-list';
import { FlexLayoutModule } from '@angular/flex-layout';

import { HomeComponent } from './components/home/home.component';
import { MenuMaestriaComponent } from './components/menus/menu-maestria/menu-maestria.component';
import { MenuDoctoradoComponent } from './components/menus/menu-doctorado/menu-doctorado.component';
import { MenuGlobalComponent } from './components/menus/menu-global/menu-global.component';
import { MenuProfesoresComponent } from './components/menus/menu-profesores/menu-profesores.component';
import { MenuTesisComponent } from './components/menus/menu-tesis/menu-tesis.component';

import {ChartModule} from 'primeng/chart';
import { BarChartComponent } from './components/graphs/bar-chart/bar-chart.component';
import { IngresoEgresoComponent } from './components/graphs/ingreso-egreso/ingreso-egreso.component';
import { EficienciaComponent } from './components/graphs/eficiencia/eficiencia.component';
import { AspirantesComponent } from './components/graphs/aspirantes/aspirantes.component';
import { ProduccionComponent } from './components/graphs/produccion/produccion.component';
import { PromedioComponent } from './components/graphs/promedio/promedio.component';
import { NumeroProfesoresComponent } from './components/graphs/numero-profesores/numero-profesores.component';
import { ProduccionProfesoresComponent } from './components/graphs/produccion-profesores/produccion-profesores.component';
import { PerfilesProfesoresComponent } from './components/graphs/perfiles-profesores/perfiles-profesores.component';
import { IcrsComponent } from './components/graphs/icrs/icrs.component';
import { TesisComponent } from './components/graphs/tesis/tesis.component';
import { IcryTesisComponent } from './components/graphs/icry-tesis/icry-tesis.component';
import { TiempoIcrTesisComponent } from './components/graphs/tiempo-icr-tesis/tiempo-icr-tesis.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    IndicatorsComponent,
    IndicatorComponent,
    MenuComponent,
    HomeComponent,
    MenuMaestriaComponent,
    MenuDoctoradoComponent,
    MenuGlobalComponent,
    MenuProfesoresComponent,
    MenuTesisComponent,
    BarChartComponent,
    IngresoEgresoComponent,
    EficienciaComponent,
    AspirantesComponent,
    ProduccionComponent,
    PromedioComponent,
    NumeroProfesoresComponent,
    ProduccionProfesoresComponent,
    PerfilesProfesoresComponent,
    IcrsComponent,
    TesisComponent,
    IcryTesisComponent,
    TiempoIcrTesisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    ChartModule,
    MatTableModule,
    MatGridListModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
  ]
})
export class AppModule { }
