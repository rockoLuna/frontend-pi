import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-doctorado',
  templateUrl: './menu-doctorado.component.html',
  styleUrls: ['./menu-doctorado.component.scss']
})
export class MenuDoctoradoComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit(): void {
  }

}
