import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  fillerNav = [
    { name: "Maestria", route: "menuMaestria", icon: "", show: true },
    { name: "Doctorado", route: "menuDoctorado", icon: "", show: true },
    { name: "Programa", route: "menuGlobal", icon: "", show: true },
    { name: "Profesores", route: "menuProfesores", icon: "", show: true },
    { name: "ICR's-Tésis", route: "menuTesis", icon: "", show: true }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
