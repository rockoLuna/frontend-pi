import { Component, Input, OnInit } from '@angular/core';
import { Indicator } from '../../model/Indicator';

@Component({
  selector: 'app-indicator',
  templateUrl: './indicator.component.html',
  styleUrls: ['./indicator.component.scss']
})
export class IndicatorComponent implements OnInit {

  @Input() indicator : Indicator = new Indicator();
  @Input() indics : Indicator[] = [];

  constructor() { }

  ngOnInit(): void {
    for (let i=0; i<this.indics.length; ++i){
      console.log("valor: " + this.indics[i].cantidad);
    }
  }

}
