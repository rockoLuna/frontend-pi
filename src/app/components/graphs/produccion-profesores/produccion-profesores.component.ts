import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { IndicatorService } from 'src/app/indicator.service';
import { ProfessorIndicator } from 'src/app/model/ProfessorIndicator';

@Component({
  selector: 'app-produccion-profesores',
  templateUrl: './produccion-profesores.component.html',
  styleUrls: ['./produccion-profesores.component.scss']
})
export class ProduccionProfesoresComponent implements OnInit {
  indicatorsOne: ProfessorIndicator[] = [];
  indicatorsTwo: ProfessorIndicator[] = [];
  indicatorsThree: ProfessorIndicator[] = [];
  indicatorsFour: ProfessorIndicator[] = [];
  indicatorsFive: ProfessorIndicator[] = [];
  indicatorsSix: ProfessorIndicator[] = [];
  //charts
  chartOptions: any;
  chartDataOne: any;
  chartDataTwo: any;
  chartDataThree: any;
  chartDataFour: any;
  chartDataFive: any;
  dataOne: number[] = [];
  dataTwo: number[] = [];
  dataThree: number[] = [];
  dataFour: number[] = [];
  dataFive: number[] = [];
  year: number[] = [];
  colors: any;
  labels: any;
  hoverColors: any;
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    this.dataSourceTab = [
      { anio: 0, articulo: 0, presentacion: 0, memoria: 0, libro: 0, patente: 0, desarrollo: 0 },
      { anio: 0, articulo: 0, presentacion: 0, memoria: 0, libro: 0, patente: 0, desarrollo: 0 },
      { anio: 0, articulo: 0, presentacion: 0, memoria: 0, libro: 0, patente: 0, desarrollo: 0 },
      { anio: 0, articulo: 0, presentacion: 0, memoria: 0, libro: 0, patente: 0, desarrollo: 0 },
      { anio: 0, articulo: 0, presentacion: 0, memoria: 0, libro: 0, patente: 0, desarrollo: 0 }
    ];
    this.displayedColumns = ['anio', 'articulo', 'presentacion', 'memoria',
      'libro', 'patente', 'desarrollo'];

    //recuperar producción de los últimos 5 años
    this.service.searchProfessorIndicators(5).subscribe(resp => {
      this.indicatorsOne = resp.indicadores;
      this.dataOne.push(this.indicatorsOne[0].cantidad);
      this.dataTwo.push(this.indicatorsOne[1].cantidad);
      this.dataThree.push(this.indicatorsOne[2].cantidad);
      this.dataFour.push(this.indicatorsOne[3].cantidad);
      this.dataFive.push(this.indicatorsOne[4].cantidad);
      //recupera años a desplegar
      let j = resp.indicadores.length - 5;
      for (let i = 0; i < 5; ++i) {
        this.year.push(this.indicatorsOne[j].generacion);
        this.dataSourceTab[i].anio = this.indicatorsOne[j].generacion;
        this.dataSourceTab[i].articulo = this.indicatorsOne[j].cantidad;
        j += 1;
      }
      this.service.searchProfessorIndicators(6).subscribe(resp => {
        this.indicatorsTwo = resp.indicadores;
        this.dataOne.push(this.indicatorsTwo[0].cantidad);
        this.dataTwo.push(this.indicatorsTwo[1].cantidad);
        this.dataThree.push(this.indicatorsTwo[2].cantidad);
        this.dataFour.push(this.indicatorsTwo[3].cantidad);
        this.dataFive.push(this.indicatorsTwo[4].cantidad);
        let j = resp.indicadores.length - 5;
        for (let i = 0; i < 5; ++i) {
          this.dataSourceTab[i].presentacion = this.indicatorsTwo[j].cantidad;
          j += 1;
        }
        this.service.searchProfessorIndicators(7).subscribe(resp => {
          this.indicatorsThree = resp.indicadores;
          this.dataOne.push(this.indicatorsThree[0].cantidad);
          this.dataTwo.push(this.indicatorsThree[1].cantidad);
          this.dataThree.push(this.indicatorsThree[2].cantidad);
          this.dataFour.push(this.indicatorsThree[3].cantidad);
          this.dataFive.push(this.indicatorsThree[4].cantidad);
          let j = resp.indicadores.length - 5;
          for (let i = 0; i < 5; ++i) {
            this.dataSourceTab[i].memoria = this.indicatorsThree[j].cantidad;
            j += 1;
          }
          this.service.searchProfessorIndicators(8).subscribe(resp => {
            this.indicatorsFour = resp.indicadores;
            this.dataOne.push(this.indicatorsFour[0].cantidad);
            this.dataTwo.push(this.indicatorsFour[1].cantidad);
            this.dataThree.push(this.indicatorsFour[2].cantidad);
            this.dataFour.push(this.indicatorsFour[3].cantidad);
            this.dataFive.push(this.indicatorsFour[4].cantidad);
            let j = resp.indicadores.length - 5;
            for (let i = 0; i < 5; ++i) {
              this.dataSourceTab[i].libro = this.indicatorsFour[j].cantidad;
              j += 1;
            }
            this.service.searchProfessorIndicators(9).subscribe(resp => {
              this.indicatorsFive = resp.indicadores;
              this.dataOne.push(this.indicatorsFive[0].cantidad);
              this.dataTwo.push(this.indicatorsFive[1].cantidad);
              this.dataThree.push(this.indicatorsFive[2].cantidad);
              this.dataFour.push(this.indicatorsFive[3].cantidad);
              this.dataFive.push(this.indicatorsFive[4].cantidad);
              let j = resp.indicadores.length - 5;
              for (let i = 0; i < 5; ++i) {
                this.dataSourceTab[i].patente = this.indicatorsFive[j].cantidad;
                j += 1;
              }
              this.service.searchProfessorIndicators(10).subscribe(resp => {
                this.indicatorsSix = resp.indicadores;
                this.dataOne.push(this.indicatorsSix[0].cantidad);
                this.dataTwo.push(this.indicatorsSix[1].cantidad);
                this.dataThree.push(this.indicatorsSix[2].cantidad);
                this.dataFour.push(this.indicatorsSix[3].cantidad);
                this.dataFive.push(this.indicatorsSix[4].cantidad);
                let j = resp.indicadores.length - 5;
                for (let i = 0; i < 5; ++i) {
                  this.dataSourceTab[i].desarrollo = this.indicatorsSix[j].cantidad;
                  j += 1;
                }
              }
              );
            }
            );
          }
          );
        }
        );
      }
      );
      console.log(this.dataSourceTab);
    }
    );

    this.labels = ['Artículo especializado', 'Presentación en evento', 'Memorias in extenso',
      'Libro', 'Patente', 'Desarrollo de software'];
    this.colors = ["#EA7B05", "#EA3205", "#EA054A", "#D2EA05", "#05EA4A", "#058CEA"];
    this.hoverColors = ["#EC8D27", "#EC512B", "#E92460", "#E0F526", "#2DF369", "#319EEA"];

    this.chartDataOne = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataOne, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataTwo = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataTwo, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataThree = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataThree, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataFour = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataFour, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataFive = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataFive, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors
        }
      ]
    };
  }

  goBack() {
    this.location.back();
  }

}
