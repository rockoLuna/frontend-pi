import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TesisIcrIndicator } from 'src/app/model/TesisIcrIndicator';
import { IndicatorService } from 'src/app/indicator.service';

@Component({
  selector: 'app-icrs',
  templateUrl: './icrs.component.html',
  styleUrls: ['./icrs.component.scss']
})
export class IcrsComponent implements OnInit {
  indicators: TesisIcrIndicator[] = [];
  //chart
  chartData: any;
  chartOptions: any;
  dataCon: number[] = [];
  dataNoCon: number[] = [];
  labelsX: number[] = [];
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    //table
    this.dataSourceTab = [
      { anio: 0, concluidas: 0, noConcluidas: 0 },
      { anio: 0, concluidas: 0, noConcluidas: 0 },
      { anio: 0, concluidas: 0, noConcluidas: 0 },
      { anio: 0, concluidas: 0, noConcluidas: 0 },
      { anio: 0, concluidas: 0, noConcluidas: 0 }
    ];
    this.displayedColumns = ['anio', 'concluidas', 'noConcluidas'];

    //recuperar concluidas y etiquetas del chart de los últimos 5 años
    this.service.searchTesisIcrIndicators('icr', 1).subscribe(resp => {
      this.indicators = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.labelsX[aux] = this.indicators[j].generacion;
        this.dataCon[aux] = this.indicators[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].anio = this.labelsX[k];
            this.dataSourceTab[k].concluidas = this.dataCon[k];
          }
        }
      }
    }
    );

    //recupera no concluidas de los últimos 5 años
    this.service.searchTesisIcrIndicators('icr', 2).subscribe(resp => {
      this.indicators = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.dataNoCon[aux] = this.indicators[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].noConcluidas = this.dataNoCon[k];
          }
        }
      }
    }
    );

    this.chartData = {
      labels: this.labelsX,
      datasets: [
        {
          label: 'Concluidas',
          backgroundColor: '#FA8072',
          data: this.dataCon
        },
        {
          label: 'No Concluidas',
          backgroundColor: '#CD5C5C',
          data: this.dataNoCon
        }
      ]
    };
  }

  goBack() {
    this.location.back();
  }

}
