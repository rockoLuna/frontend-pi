import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TesisIcrIndicator } from 'src/app/model/TesisIcrIndicator';
import { IndicatorService } from 'src/app/indicator.service';

@Component({
  selector: 'app-tiempo-icr-tesis',
  templateUrl: './tiempo-icr-tesis.component.html',
  styleUrls: ['./tiempo-icr-tesis.component.scss']
})
export class TiempoIcrTesisComponent implements OnInit {
  indicatorsTime: TesisIcrIndicator[] = [];
  //chart
  chartData: any;
  chartOptions: any;
  dataIcr: number[] = [];
  dataTesis: number[] = [];
  labelsX: number[] = [];
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    this.dataSourceTab = [
      { anio: 0, icr: 0, tesis: 0 },
      { anio: 0, icr: 0, tesis: 0 },
      { anio: 0, icr: 0, tesis: 0 },
      { anio: 0, icr: 0, tesis: 0 },
      { anio: 0, icr: 0, tesis: 0 },
    ];
    this.displayedColumns = ['anio', 'icr', 'tesis'];

    //recuperar promedio icr y etiquetas del chart de los últimos 5 años
    this.service.searchTesisIcrIndicators('icr', 3).subscribe(resp => {
      this.indicatorsTime = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.labelsX[aux] = this.indicatorsTime[j].generacion;
        this.dataIcr[aux] = this.indicatorsTime[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].anio = this.labelsX[k];
            this.dataSourceTab[k].icr = this.dataIcr[k];
          }
        }
      }    
    }
    );

    //recuperar promedio tesis
    this.service.searchTesisIcrIndicators('tesis', 3).subscribe(resp => {
      this.indicatorsTime = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.dataTesis[aux] = this.indicatorsTime[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].tesis = this.dataTesis[k];
          }
        }
      }    
    }
    );
    this.chartData = {
      labels: this.labelsX,
      datasets: [
        {
          type: 'line', label: 'ICR',
          data: this.dataIcr, fill: true,
          borderColor: '#07969B', tension: .4,
          backgroundColor: 'rgba(6,143,150,0.2)'
        },
        {
          type: 'line', label: 'Tésis', 
          data: this.dataTesis, fill: true,
          borderColor: '#E9967A', tension: .4,
          backgroundColor: 'rgba(233, 150, 122,0.2)'
        }
      ]
    };
    this.chartOptions = {
      yAxes: [{
        ticks: {
          stepSize: 100,
          beginAtZero: true
        }
      }]
    };
  }

  goBack() {
    this.location.back();
  }

}
