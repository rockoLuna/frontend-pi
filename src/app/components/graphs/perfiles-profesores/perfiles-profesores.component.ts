import { Component, OnInit } from '@angular/core';
import { IndicatorService } from 'src/app/indicator.service';
import { Location } from '@angular/common';
import { ProfessorIndicator } from 'src/app/model/ProfessorIndicator';

@Component({
  selector: 'app-perfiles-profesores',
  templateUrl: './perfiles-profesores.component.html',
  styleUrls: ['./perfiles-profesores.component.scss']
})
export class PerfilesProfesoresComponent implements OnInit {
  indicatorsSNI: ProfessorIndicator[] = [];
  indicatorsPRO: ProfessorIndicator[] = [];
  indicatorsSNC: ProfessorIndicator[] = [];
  //charts 
  chartDataOne: any;
  chartDataTwo: any;
  chartDataThree: any;
  chartDataFour: any;
  chartDataFive: any;
  chartOptions: any;
  dataOne: number[] = [];
  dataTwo: number[] = [];
  dataThree: number[] = [];
  dataFour: number[] = [];
  dataFive: number[] = [];
  year: number[] = [];
  colors: any;
  labels: any;
  hoverColors: any;
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];


  constructor(private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    //table
    this.dataSourceTab = [
      { anio: 0, sni: 0, prodep: 0, snc: 0 },
      { anio: 0, sni: 0, prodep: 0, snc: 0 },
      { anio: 0, sni: 0, prodep: 0, snc: 0 },
      { anio: 0, sni: 0, prodep: 0, snc: 0 },
      { anio: 0, sni: 0, prodep: 0, snc: 0 }
    ];
    this.displayedColumns = ['anio', 'sni', 'prodep', 'snc'];

    //recuperar pertenecientes al SNI
    this.service.searchProfessorIndicators(11).subscribe(resp => {
      this.indicatorsSNI = resp.indicadores;
      this.dataOne.push(this.indicatorsSNI[0].cantidad);
      this.dataTwo.push(this.indicatorsSNI[1].cantidad);
      this.dataThree.push(this.indicatorsSNI[2].cantidad);
      this.dataFour.push(this.indicatorsSNI[3].cantidad);
      this.dataFive.push(this.indicatorsSNI[4].cantidad);
      //recupera años a desplegar
      let j = resp.indicadores.length - 5;
      for (let i = 0; i < 5; ++i) {
        this.year.push(this.indicatorsSNI[j].generacion);
        this.dataSourceTab[i].anio = this.indicatorsSNI[j].generacion;
        this.dataSourceTab[i].sni = this.indicatorsSNI[j].cantidad;
        j += 1;
      }
      //recuperar con perfil prodep
      this.service.searchProfessorIndicators(12).subscribe(resp => {
        this.indicatorsPRO = resp.indicadores;
        this.dataOne.push(this.indicatorsPRO[0].cantidad);
        this.dataTwo.push(this.indicatorsPRO[1].cantidad);
        this.dataThree.push(this.indicatorsPRO[2].cantidad);
        this.dataFour.push(this.indicatorsPRO[3].cantidad);
        this.dataFive.push(this.indicatorsPRO[4].cantidad);
        let j = resp.indicadores.length - 5;
        for (let i = 0; i < 5; ++i) {
          this.dataSourceTab[i].prodep = this.indicatorsPRO[j].cantidad;
          j += 1;
        }
        //recuperar pertenecientes al SNC
        this.service.searchProfessorIndicators(13).subscribe(resp => {
          this.indicatorsSNC = resp.indicadores;
          this.dataOne.push(this.indicatorsSNC[0].cantidad);
          this.dataTwo.push(this.indicatorsSNC[1].cantidad);
          this.dataThree.push(this.indicatorsSNC[2].cantidad);
          this.dataFour.push(this.indicatorsSNC[3].cantidad);
          this.dataFive.push(this.indicatorsSNC[4].cantidad);
          let j = resp.indicadores.length - 5;
          for (let i = 0; i < 5; ++i) {
            this.dataSourceTab[i].snc = this.indicatorsSNC[j].cantidad;
            j += 1;
          }
        }
        );
      }
      );
    }
    );

    this.colors = ["#F1BD33", "#E8774D", "#4B9EB2"];
    this.hoverColors = ["#E7B021", "#EA5D28", "#1D8FAB"];
    this.labels = ['SNI', 'Prodep', 'SNC'];

    this.chartDataOne = {
      labels: this.labels,
      datasets: [
        { data: this.dataOne, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors}
      ]
    };
    this.chartDataTwo = {
      labels: this.labels,
      datasets: [
        { data: this.dataTwo, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors}
      ]
    };
    this.chartDataThree = {
      labels: this.labels,
      datasets: [
        { data: this.dataThree, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors }
      ]
    };
    this.chartDataFour = {
      labels: this.labels,
      datasets: [
        { data: this.dataFour, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors }
      ]
    };
    this.chartDataFive = {
      labels: this.labels,
      datasets: [
        { data: this.dataFive, backgroundColor: this.colors, hoverBackgroundColor: this.hoverColors }
      ]
    };
  }

  goBack() {
    this.location.back();
  }
}
