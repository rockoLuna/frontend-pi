import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { IndicatorService } from 'src/app/indicator.service';
import { Indicator } from 'src/app/model/Indicator';

@Component({
  selector: 'app-aspirantes',
  templateUrl: './aspirantes.component.html',
  styleUrls: ['./aspirantes.component.scss']
})
export class AspirantesComponent implements OnInit {
  level: any;
  indicatorsAsp: Indicator[] = [];
  indicatorsAce: Indicator[] = [];
  //charts 
  chartDataOne: any;
  chartDataTwo: any;
  chartDataThree: any;
  chartDataFour: any;
  chartDataFive: any;
  chartOptions: any;
  dataOne: number[] = [];
  dataTwo: number[] = [];
  dataThree: number[] = [];
  dataFour: number[] = [];
  dataFive: number[] = [];
  year: number[] = [];
  colors: any;
  labels: any;
  hoverColors : any;
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private router: Router, private route: ActivatedRoute,
    private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    this.level = this.route.snapshot.paramMap.get('level');
    //table
    this.dataSourceTab = [
      { anio: 0, aspirantes: 0, aceptados: 0 },
      { anio: 0, aspirantes: 0, aceptados: 0 },
      { anio: 0, aspirantes: 0, aceptados: 0 },
      { anio: 0, aspirantes: 0, aceptados: 0 },
      { anio: 0, aspirantes: 0, aceptados: 0 },
    ];
    this.displayedColumns = ['anio', 'aspirantes', 'aceptados'];

    //recuperar aspirantes y etiquetas del chart de los últimos 5 años
    this.service.searchIndicators(this.level, 4).subscribe(resp => {
      this.indicatorsAsp = resp.indicadores;
      this.dataOne.push(this.indicatorsAsp[0].cantidad);
      this.dataTwo.push(this.indicatorsAsp[1].cantidad);
      this.dataThree.push(this.indicatorsAsp[2].cantidad);
      this.dataFour.push(this.indicatorsAsp[3].cantidad);
      this.dataFive.push(this.indicatorsAsp[4].cantidad);
      //recupera años a desplegar
        let j = resp.indicadores.length - 5;
        for (let i = 0; i < 5; ++i) {          
          this.year.push(this.indicatorsAsp[j].generacion);          
          this.dataSourceTab[i].anio = this.indicatorsAsp[j].generacion;
          this.dataSourceTab[i].aspirantes = this.indicatorsAsp[j].cantidad;
          j += 1;
        }        
      //recuperar aceptados de los últimos 5 años
      this.service.searchIndicators(this.level, 5).subscribe(resp => {
        this.indicatorsAce = resp.indicadores;
        this.dataOne.push(this.indicatorsAce[0].cantidad);
        this.dataTwo.push(this.indicatorsAce[1].cantidad);
        this.dataThree.push(this.indicatorsAce[2].cantidad);
        this.dataFour.push(this.indicatorsAce[3].cantidad);
        this.dataFive.push(this.indicatorsAce[4].cantidad);
        let j = resp.indicadores.length - 5;
        for (let i = 0; i < 5; ++i) {          
          this.dataSourceTab[i].aceptados = this.indicatorsAce[j].cantidad;
          j += 1;
        }
      }
      );
    }
    );

    this.colors = ["#CD5C5C", "#FFA07A"];
    this.hoverColors = ["#E56161", "#FD966C"];
    this.labels = ['Aspirantes', 'Aceptados'];

    this.chartDataOne = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataOne,
          backgroundColor: this.colors,
          hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataTwo = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataTwo,
          backgroundColor: this.colors,
          hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataThree = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataThree,
          backgroundColor: this.colors,
          hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataFour = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataFour,
          backgroundColor: this.colors,
          hoverBackgroundColor: this.hoverColors
        }
      ]
    };
    this.chartDataFive = {
      labels: this.labels,
      datasets: [
        {
          data: this.dataFive,
          backgroundColor: this.colors,
          hoverBackgroundColor: this.hoverColors
        }
      ]
    };
  }

  goBack() {
    this.location.back();
  }

}