import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { IndicatorService } from 'src/app/indicator.service';
import { ProfessorIndicator } from 'src/app/model/ProfessorIndicator';


@Component({
  selector: 'app-numero-profesores',
  templateUrl: './numero-profesores.component.html',
  styleUrls: ['./numero-profesores.component.scss']
})
export class NumeroProfesoresComponent implements OnInit {
  indicators: ProfessorIndicator[] = [];
  //chart
  chartDataOne: any;
  chartDataTwo: any;
  chartOptions: any;
  dataNB: number[] = [];
  dataPC: number[] = [];
  dataEP: number[] = [];
  dataCP: number[] = [];
  labelsX: number[] = [];
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    //table
    this.dataSourceTab = [
      { anio: 0, nucleo: 0, complementaria: 0, exclusivos: 0, compartidos: 0 },
      { anio: 0, nucleo: 0, complementaria: 0, exclusivos: 0, compartidos: 0 },
      { anio: 0, nucleo: 0, complementaria: 0, exclusivos: 0, compartidos: 0 },
      { anio: 0, nucleo: 0, complementaria: 0, exclusivos: 0, compartidos: 0 },
      { anio: 0, nucleo: 0, complementaria: 0, exclusivos: 0, compartidos: 0 },
    ];
    this.displayedColumns = ['anio', 'nucleo', 'complementaria', 'exclusivos', 'compartidos'];

    //recuperar núcleo básico y etiquetas del chart de los últimos 5 años
    this.service.searchProfessorIndicators(1).subscribe(resp => {
      this.indicators = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.labelsX[aux] = this.indicators[j].generacion;
        this.dataNB[aux] = this.indicators[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].anio = this.labelsX[k];
            this.dataSourceTab[k].nucleo = this.dataNB[k];
          }
        }        
      }
      console.log(this.dataNB);
    }    
    );

    //recuperar planta complementaria
    this.service.searchProfessorIndicators(2).subscribe(resp => {
      this.indicators = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.dataPC[aux] = this.indicators[j].cantidad;
        aux -= 1;
        j -= 1;
        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].complementaria = this.dataPC[k];
          }
        }
      }
      console.log(this.dataPC);
    }    
    );
    //recuperar exclusivos del programa
    this.service.searchProfessorIndicators(3).subscribe(resp => {
      this.indicators = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.dataEP[aux] = this.indicators[j].cantidad;
        aux -= 1;
        j -= 1;
        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].exclusivos = this.dataEP[k];
          }
        }
      }
      console.log(this.dataEP);
    }    
    );
    //recuperar compartidos conotros programas
    this.service.searchProfessorIndicators(4).subscribe(resp => {
      this.indicators = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.dataCP[aux] = this.indicators[j].cantidad;
        aux -= 1;
        j -= 1;
        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].compartidos = this.dataCP[k];
          }
        }
      }
      console.log(this.dataCP);
    }    
    );

    this.chartDataOne = {
      labels: this.labelsX,
      datasets: [
        {
          label: 'Núcleo Básico',
          backgroundColor: '#C39BD3',
          data: this.dataNB
        },
        {
          label: 'Planta Complementaria',
          backgroundColor: '#8E44AD',
          data: this.dataPC
        }
      ]
    };
    this.chartDataTwo = {
      labels: this.labelsX,
      datasets: [
        {
          label: 'Programa exclusivo',
          backgroundColor: '#27AE60',
          data: this.dataEP
        },
        {
          label: 'Programa Compartido',
          backgroundColor: '#82E0AA',
          data: this.dataCP
        }
      ]
    };    
  }

  goBack() {
    this.location.back();
  }
}
