import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IndicatorService } from 'src/app/indicator.service';
import { Indicator } from 'src/app/model/Indicator';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  type: any;
  level: any;
  indicatorsIn: Indicator[] = [];
  indicatorsEg: Indicator[] = [];
  //chart
  chartData: any;
  chartOptions: any;
  dataIn: number[] = [];
  dataEg: number[] = [];
  labelsX: number[] = [];
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private router: Router, private route: ActivatedRoute,
    private service: IndicatorService, private location: Location) {
  }

  ngOnInit(): void {
    this.type = this.route.snapshot.paramMap.get('type');
    this.level = this.route.snapshot.paramMap.get('level');
    //table
    this.dataSourceTab = [
      { anio: 0, ingresos: 0, egresos: 0 },
      { anio: 0, ingresos: 0, egresos: 0 },
      { anio: 0, ingresos: 0, egresos: 0 },
      { anio: 0, ingresos: 0, egresos: 0 },
      { anio: 0, ingresos: 0, egresos: 0 },
    ];
    this.displayedColumns = ['anio', 'ingresos', 'egresos'];

    //recuperar ingresos y etiquetas del chart de los últimos 5 años
    this.service.searchIndicators(this.level, this.type).subscribe(resp => {
      this.indicatorsIn = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.labelsX[aux] = this.indicatorsIn[j].generacion;
        this.dataIn[aux] = this.indicatorsIn[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].anio = this.labelsX[k];
            this.dataSourceTab[k].ingresos = this.dataIn[k];
          }
        }
      }
    }
    );

    //recupera egresos de los últimos 5 años
    this.service.searchIndicators(this.level, Number(this.type) + 1).subscribe(resp => {
      this.indicatorsEg = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.dataEg[aux] = this.indicatorsEg[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].egresos = this.dataEg[k];
          }
        }
      }
    }
    );

    this.chartData = {
      labels: this.labelsX,
      datasets: [
        {
          label: 'Ingresos',
          backgroundColor: '#86FF33',
          data: this.dataIn
        },
        {
          label: 'Egresos',
          backgroundColor: '#33A8FF',
          data: this.dataEg
        }
      ]
    };
  }

  goBack() {
    this.location.back();
  }
}
