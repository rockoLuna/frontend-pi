import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { IndicatorService } from 'src/app/indicator.service';
import { Indicator } from 'src/app/model/Indicator';

@Component({
  selector: 'app-promedio',
  templateUrl: './promedio.component.html',
  styleUrls: ['./promedio.component.scss']
})
export class PromedioComponent implements OnInit {
  level: any;
  indicatorsProm: Indicator[] = [];
  //chart
  chartData: any;
  chartOptions: any;
  dataProm: number[] = [];
  labelsX: number[] = [];
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private router: Router, private route: ActivatedRoute,
    private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    this.level = this.route.snapshot.paramMap.get('level');
    this.dataSourceTab = [
      { anio: 0, promedio: 0 },
      { anio: 0, promedio: 0 },
      { anio: 0, promedio: 0 },
      { anio: 0, promedio: 0 },
      { anio: 0, promedio: 0 },
    ];
    this.displayedColumns = ['anio', 'promedio'];

    //recuperar promedio y etiquetas del chart de los últimos 5 años
    this.service.searchIndicators(this.level, 12).subscribe(resp => {
      this.indicatorsProm = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.labelsX[aux] = this.indicatorsProm[j].generacion;
        this.dataProm[aux] = this.indicatorsProm[j].cantidad / 10;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].anio = this.labelsX[k];
            this.dataSourceTab[k].promedio = this.dataProm[k];
          }
        }
      }

      this.chartData = {
        labels: this.labelsX,
        datasets: [
          {
            type: 'line',
            label: 'Promedio',
            data: this.dataProm,
            fill: true,
            borderColor: '#07969B',
            tension: .4,
            backgroundColor: 'rgba(6,143,150,0.2)'
          }
        ]
      };
      this.chartOptions = {
        yAxes: [{
          ticks: {
            stepSize: 100,
            beginAtZero: true
          }
        }]
      };
    }
    );
  }

  goBack() {
    this.location.back();
  }

}
