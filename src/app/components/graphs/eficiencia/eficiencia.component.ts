import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IndicatorService } from 'src/app/indicator.service';
import { Indicator } from 'src/app/model/Indicator';

@Component({
  selector: 'app-eficiencia',
  templateUrl: './eficiencia.component.html',
  styleUrls: ['./eficiencia.component.scss']
})
export class EficienciaComponent implements OnInit {
  level: any;
  indicatorsEf: Indicator[] = [];
  //chart
  chartData: any;
  chartOptions: any;
  dataEf: number[] = [];
  labelsX: number[] = [];
  //table
  displayedColumns: string[] = []
  dataSourceTab: any[] = [];

  constructor(private router: Router, private route: ActivatedRoute,
    private service: IndicatorService, private location: Location) { }

  ngOnInit(): void {
    this.level = this.route.snapshot.paramMap.get('level');
    this.dataSourceTab = [
      { anio: 0, eficiencia: 0 },
      { anio: 0, eficiencia: 0 },
      { anio: 0, eficiencia: 0 },
      { anio: 0, eficiencia: 0 },
      { anio: 0, eficiencia: 0 },
    ];
    this.displayedColumns = ['anio', 'eficiencia'];

    //recuperar eficiencia y etiquetas del chart de los últimos 5 años
    this.service.searchIndicators(this.level, 3).subscribe(resp => {
      this.indicatorsEf = resp.indicadores;
      let aux = 4;
      let j = resp.indicadores.length - 1;
      for (let i = 0; i < 5; ++i) {
        this.labelsX[aux] = this.indicatorsEf[j].generacion;
        this.dataEf[aux] = this.indicatorsEf[j].cantidad;
        aux -= 1;
        j -= 1;

        if (i === 4) {
          for (let k = 0; k < 5; ++k) {
            this.dataSourceTab[k].anio = this.labelsX[k];
            this.dataSourceTab[k].eficiencia = this.dataEf[k];
          }
        }
      }

      this.chartData = {
        labels: this.labelsX,
        datasets: [
          {
            label: '% de terminación por año',
            data: this.dataEf,
            fill: true,
            borderColor: '#FFA726',
            tension: .4,
            backgroundColor: 'rgba(255,167,38,0.2)'
          }
        ]
      };
      this.chartOptions = {
        yAxes: [{
          ticks: {
            stepSize: 100,
            beginAtZero: true
          }
        }]

      };


    }
    );
  }

  goBack() {
    this.location.back();
  }

}
