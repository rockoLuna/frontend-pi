import { Component, OnInit } from '@angular/core';
import { Indicator } from '../../model/Indicator';
import { IndicatorService } from '../../indicator.service';


@Component({
  selector: 'app-indicators',
  templateUrl: './indicators.component.html',
  styleUrls: ['./indicators.component.scss']
})
export class IndicatorsComponent implements OnInit {

  indicators: Indicator[] = [];
  //indicartorsFilter: Indicator[] = [];

  constructor(private service: IndicatorService) { }

  ngOnInit(): void {
    /*
    this.service.getIndicators().subscribe(resp => {
        this.indicators = resp.indicadores;
        console.log(this.indicators);
      }
    );
    */

    this.service.searchIndicators("maestria", 1).subscribe(resp =>{
        this.indicators = resp.indicadores;
        console.log(this.indicators);
      }
    );

    
  }
}


